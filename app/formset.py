from django.forms.formsets import BaseFormSet
from .models import *


class ChecklistFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ChecklistFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class CheckFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(CheckFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ArrivalFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ArrivalFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ItemTransferFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ItemTransferFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ProcessItemFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ProcessItemFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ProcessFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ProcessFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False



class ProcessFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ProcessFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class ItemExpenseFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ItemExpenseFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

class ExpenseFormset(BaseFormSet):
    def clean(self):
        if any(self.errors):
            return

    def __init__(self, *args, **kwargs):
        super(ExpenseFormset, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

