from django import forms
from django.forms import fields, models, formsets, widgets
from django.forms import BaseFormSet, formset_factory, BaseInlineFormSet
from .models import *


class ChecklistFieldForm(forms.ModelForm):
    class Meta:
        model = ChecklistField
        fields = ['item', 'display_order']

    def __init__(self, *args, **kwargs):
        super(ChecklistFieldForm, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['class'] = 'form-control'
        self.fields['display_order'].widget.attrs['class'] = 'form-control'


class ChecklistForm(forms.ModelForm):
    class Meta:
        model = Checklist
        fields = ['name']

    def __init__(self, *args, **kwargs):
        super(ChecklistForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'


class ArrivalForm(forms.ModelForm):
    class Meta:
        model = Arrival
        fields = ['receipt_no','supplier', 'type']

    def __init__(self, *args, **kwargs):
        super(ArrivalForm, self).__init__(*args, **kwargs)
        self.fields['receipt_no'].widget.attrs['class'] = 'form-control'
        self.fields['supplier'].widget.attrs['class'] = 'form-control'
        self.fields['type'].widget.attrs['class'] = 'form-control'


class ItemArrivalForm(forms.ModelForm):
    class Meta:
        model = ItemArrival
        fields = ['item', 'quantity']

    def __init__(self, *args, **kwargs):
        super(ItemArrivalForm, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['class'] = 'form-control'
        self.fields['quantity'].widget.attrs['class'] = 'form-control'


class TransferForm(forms.ModelForm):
    class Meta:
        model = Transfer
        fields = ['To']

    def __init__(self, *args, **kwargs):
        super(TransferForm, self).__init__(*args, **kwargs)
        locations = Location.objects.filter(is_active=True)
        self.fields['To'].widget.attrs['class'] = 'form-control'
        self.fields['To'].queryset = locations.exclude(name="Warehouse")


class ItemTransferForm(forms.ModelForm):
    class Meta:
        model = ItemTransfer
        fields = ['item', 'quantity']

    def __init__(self, *args, **kwargs):
        super(ItemTransferForm, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['class'] = 'form-control'
        self.fields['quantity'].widget.attrs['class'] = 'form-control'


class ProcessItemForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = ['item']
        exclude = fields
    #
    # def __init__(self, *args, **kwargs):
    #
    #     super(ProcessItemForm, self).__init__(*args, **kwargs)
    #     all_items = Item.objects.filter(is_active=True)
    #     self.fields['item'].widget.attrs['class'] = 'form-control'
    #     self.fields['item'].widget.attrs['disabled'] = 'disabled'
    #     # self.fields['item'].queryset = all_items.filter(type="processed")


class IngredientsForm(forms.ModelForm):
    class Meta:
        model = Ingredients
        fields = ['item']

    def __init__(self, *args, **kwargs):
        super(IngredientsForm, self).__init__(*args, **kwargs)
        items = Item.objects.filter(is_active=True)
        self.fields['item'].widget.attrs['class'] = 'form-control'
        self.fields['item'].queryset = items.exclude(type="Processed")

class ProcessForm(forms.ModelForm):
    class Meta:
        model = Process
        fields = ['quantity_yield']

    def __init__(self, *args, **kwargs):
        super(ProcessForm, self).__init__(*args, **kwargs)
        all_items = Item.objects.filter(is_active=True)
        self.fields['quantity_yield'].widget.attrs['class'] = 'form-control'


class ItemIngredientForm(forms.ModelForm):
    class Meta:
        model = ItemIngredient
        fields = ['item', 'quantity']

    def __init__(self, *args, **kwargs):
        super(ItemIngredientForm, self).__init__(*args, **kwargs)
        all_items = Item.objects.filter(is_active=True)
        self.fields['item'].widget.attrs['class'] = 'form-control'
        # self.fields['item'].widget.attrs['disabled'] = 'disabled'
        self.fields['quantity'].widget.attrs['class'] = 'form-control'



class ExpenseForm(forms.ModelForm):
    class Meta:
        model = Expenses
        fields = ['expenses', 'amount']

    def __init__(self, *args, **kwargs):
        super(ExpenseForm, self).__init__(*args, **kwargs)
        self.fields['expenses'].widget.attrs['class'] = 'form-control'
        self.fields['amount'].widget.attrs['class'] = 'form-control'


class ReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['name','location']

    def __init__(self, *args, **kwargs):
        super(ReportForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['location'].widget.attrs['class'] = 'form-control'



class ItemExpenseForm(forms.ModelForm):
    class Meta:
        model = ItemExpense
        fields = ['item', 'end']

    def __init__(self, *args, **kwargs):
        super(ItemExpenseForm, self).__init__(*args, **kwargs)
        self.fields['item'].widget.attrs['class'] = 'form-control'
        self.fields['end'].widget.attrs['class'] = 'form-control'
