# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from app.models import *
from app.forms import *
from app.formset import *

from django.template.loader import get_template
from .utils import render_to_pdf
from app.decorators import *
from django.forms import modelformset_factory
from django.contrib import messages
from datetime import datetime, timedelta

@login_required(login_url='/login/')
def index(request):
    return render(request, 'app/index.html')


def user_login(request):
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return HttpResponseRedirect(reverse('index'))

        except UserAccount.DoesNotExist:
            return HttpResponseRedirect(reverse('login'))

    elif request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))

    return render(request, 'app/login.html')


@login_required(login_url='/login/')
@user_can_view
def create_account(request, employee_id):
    user = Employee.objects.get(pk=employee_id)

    if request.method == 'POST':
        user_name = request.POST.get('user_name''')
        fname = request.POST.get('fname', '')
        m_initial = request.POST.get('m_initial', '')
        lname = request.POST.get('lname', '')
        gender = request.POST.get('gender', '')
        password = request.POST.get('password', '')
        can_print = request.POST.get('can_print', '')
        can_encode = request.POST.get('can_encode', '')
        position = request.POST.get('position', '')

        new_user = UserAccount.objects.create_user(firstname=fname, middle_initial=m_initial, lastname=lname,
                                                   gender=gender, position=position, username=user_name,
                                                   password=password)
        new_user.save()
        user_id = new_user.id
        saved_user = UserAccount.objects.get(pk=user_id)

        saved_user.can_print = can_print
        saved_user.can_encode = can_encode
        saved_user.save()

        user.has_account = True
        user.save()

        return HttpResponseRedirect(reverse('employees_list'))

    return render(request, 'app/create_user.html', {
        'user': user
    })


@login_required(login_url='/login/')
def user_logout(request):
    if not request.user.is_admin:
        user = request.user.id
        i = UserAccount.objects.get(id=user)
        i.can_update_report = False
        i.can_update_arrival = False
        i.save()

    logout(request)

    return HttpResponseRedirect(reverse('login'))


@login_required(login_url='/login/')
def item_page(request):
    category = Categories.objects.filter(is_active=True)
    locations = Location.objects.filter(is_active=True)
    success = False
    error = False

    if request.method == 'POST':
        try:
            item_name = request.POST.get('item_name', '')
            item_price = request.POST.get('item_price', '')
            item_type = request.POST.get('item_type', '')
            item_category = request.POST.get('item_category', '')

            if item_name and item_price and item_type and item_category:
                category_instance = Categories.objects.get(id=item_category)
                item = Item.objects.create(name=item_name, price=item_price,
                                           type=item_type, category=category_instance)
                item.save()

                for loc in locations:
                    if loc.name == "WAREHOUSE":
                        current_stock = 0
                        i = ItemLocation(item=item, location=loc, current_stock=current_stock)
                        i.save()

            success = True
        except Exception as e:
            error = True

    items = Item.objects.filter(is_active=True)

    return render(request, 'app/items.html', {
        'category': category,
        'items': items,
        'success': success,
        'error': error
    })


@login_required(login_url='/login/')
@user_can_view
def update_item(request, item_id):
    item = Item.objects.get(pk=item_id)
    categories = Categories.objects.filter(is_active=True)

    if request.method == 'POST':
        item.name = request.POST.get('update_item_name', '')
        item.price = request.POST.get('update_item_price', '')
        item.type = request.POST.get('update_item_type', '')
        item_category = request.POST.get('update_item_category', '')

        if item_category:
            item.category = Categories.objects.get(id=item_category)
            item.save()

        success = True
        return HttpResponseRedirect(reverse('list_items'))

    return render(request, 'app/update_item.html', {
        'item': item,
        'categories': categories
    })


@login_required(login_url='/login/')
@user_can_view
def delete_item(request, item_id):
    item = Item.objects.get(pk=item_id)

    item.is_active = False
    item.save()

    return HttpResponseRedirect(reverse('list_items'))


@login_required(login_url='/login/')
def location_page(request):
    items = Item.objects.filter(is_active=True)
    success = False
    error = False

    if request.method == 'POST':
        try:
            loc_name = request.POST.get('loc_name', '')
            loc_address = request.POST.get('loc_address', '')

            if loc_name and loc_address:
                location = Location.objects.create(name=loc_name, address=loc_address)
                location.save()

                if loc_name == "WAREHOUSE":
                    for item in items:
                        i = ItemLocation.objects.create(item=item, location=location, current_stock=0)
                        i.save()

            success = True
        except Exception as e:
            error = True

    locations = Location.objects.filter(is_active=True)
    return render(request, 'app/location.html', {
        'locations': locations,
        'success': success,
        'error': error
    })


@login_required(login_url='/login/')
@user_can_view
def delete_location(request, loc_id):
    location = Location.objects.get(pk=loc_id)

    location.is_active = False
    location.save()

    return HttpResponseRedirect(reverse('location_list'))


@login_required(login_url='/login/')
@user_can_view
def update_location(request, loc_id):
    location = Location.objects.get(pk=loc_id)

    if request.method == 'POST':
        location.name = request.POST.get('update_location_name', '')
        location.address = request.POST.get('update_location_address', '')

        if location.name and location.address:
            location.save()

        return HttpResponseRedirect(reverse('location_list'))

    return render(request, 'app/update_location.html', {
        'location': location,
    })


@login_required(login_url='/login/')
def suppliers(request):
    success = False
    error = False

    if request.method == 'POST':
        try:
            supplier_name = request.POST.get('supplier_name', '')
            supplier_address = request.POST.get('supplier_address', '')
            supplier_no = request.POST.get('supplier_no', '')

            supplier = Supplier.objects.create(name=supplier_name, address=supplier_address, phone=supplier_no)
            supplier.save()

            success = True

        except Exception as e:
            error = True

    supplier_list = Supplier.objects.filter(is_active=True)

    return render(request, 'app/supplier.html', {
        'suppliers': supplier_list,
        'success': success,
        'error': error
    })


@login_required(login_url='/login/')
@user_can_view
def update_supplier(request, supplier_id):
    supplier = Supplier.objects.get(pk=supplier_id)

    if request.method == 'POST':
        supplier.name = request.POST.get('update_supplier_name', '')
        supplier.address = request.POST.get('update_supplier_address', '')
        supplier.phone = request.POST.get('update_supplier_no', '')
        supplier.save()

        return HttpResponseRedirect(reverse('suppliers'))

    return render(request, 'app/update_supplier.html', {
        'supplier': supplier,
    })


@login_required(login_url='/login/')
@user_can_view
def delete_supplier(request, supplier_id):
    supplier = Supplier.objects.get(pk=supplier_id)

    supplier.is_active = False
    supplier.save()

    return HttpResponseRedirect(reverse('suppliers'))


@login_required(login_url='/login/')
def categories(request):
    success = False
    error = False

    if request.method == 'POST':
        try:
            item_category = request.POST.get('category_name', '')

            category = Categories.objects.create(name=item_category)
            category.save()

            success = True
        except Exception as e:
            error = True

    category_list = Categories.objects.filter(is_active=True)

    return render(request, 'app/categories.html', {
        'categories': category_list,
        'success': success,
        'error': error
    })


@login_required(login_url='/login/')
@user_can_view
def update_category(request, category_id):
    category = Categories.objects.get(pk=category_id)

    if request.method == 'POST':
        try:
            category.name = request.POST.get('update_category_name', '')
            category.save()
        except:
            pass

        return HttpResponseRedirect(reverse('categories'))

    return render(request, 'app/update_category.html', {
        'category': category,
    })


@login_required(login_url='/login/')
@user_can_view
def delete_category(request, category_id):
    category = Categories.objects.get(pk=category_id)

    category.is_active = False
    category.save()

    return HttpResponseRedirect(reverse('categories'))


@login_required(login_url='/login/')
def checklist_page(request):
    list = Checklist.objects.all()
    return render(request, 'app/checklist.html', {
        'list': list
    })


@login_required(login_url='/login/')
def specific_checklist(request, check_id):
    checklist_items = ChecklistField.objects.filter(checklist_id=check_id)

    return render(request, 'app/view_checklist.html', {
        'checklist_items': checklist_items
    })


@login_required(login_url='/login/')
@user_can_encode
def add_checklist(request):
    items = Item.objects.all()
    checklistForm = ChecklistForm(request.POST or None)
    formset = formset_factory(ChecklistFieldForm, formset=ChecklistFormset)
    checklistFormset = formset(request.POST or None)
    success = False

    if checklistForm.is_valid() and checklistFormset.is_valid():
        form1 = checklistForm.save(commit=False)
        check_name = checklistForm.cleaned_data['name']
        form1.save()
        check_id = form1

        for form in checklistFormset:
            checklist = check_id
            c_item = form.cleaned_data['item']
            display_order = form.cleaned_data['display_order']
            i = ChecklistField.objects.create(item=c_item, display_order=display_order, checklist=checklist)
            i.save()
            success = True

        return HttpResponseRedirect(reverse('checklist'))

    return render(request, 'app/add_checklist.html', {
        'items': items,
        'formset': checklistFormset,
        'ChecklistForm': checklistForm,
        'success': success
    })


@login_required(login_url='/login/')
@user_can_view
def delete_checklist(request, check_id):
    checklist = Checklist.objects.get(pk=check_id)
    checklist.delete()

    return HttpResponseRedirect(reverse('checklist'))


@login_required(login_url='/login/')
@user_can_encode
def update_checklist(request, check_id):
    checklist = Checklist.objects.get(id=check_id)
    check_items = ChecklistField.objects.filter(checklist_id=check_id)
    CheckFormset = modelformset_factory(ChecklistField,form=ChecklistFieldForm, fields=('item','display_order'), extra=0)
    data = request.POST or None
    formset = CheckFormset(data=data, queryset=check_items, prefix='old_check')

    formset1 = formset_factory(ChecklistFieldForm, formset=ChecklistFormset)
    checkitemFormset = formset1(request.POST or None)

    if request.method == "POST" and formset.is_valid():
        formset.save()

        if checkitemFormset.is_valid():
            for form in checkitemFormset:
                item = form.cleaned_data['item']
                order = form.cleaned_data['display_order']
                i = ChecklistField.objects.create(item=item, display_order=order, checklist_id=check_id)
                i.save()

        return HttpResponseRedirect(reverse('checklist'))

    return render(request,'app/update_checklist.html', {
        'formset': formset,
        'formset1': checkitemFormset
    })



def delete_check(request, item_id):
    check_item = ChecklistField.objects.get(item_id=item_id)
    check_id = check_item.checklist_id
    check_item.delete()

    return HttpResponseRedirect(reverse('update_checklist', args=(check_id,)))




@login_required(login_url='/login/')
def transfer_list(request):
    transfers = Transfer.objects.filter(is_active=True)

    return render(request, 'app/transfer.html', {
        'transfers': transfers
    })


@login_required(login_url='/login/')
def specific_transferItems(request, transfer_id):
    transfer_items = ItemTransfer.objects.filter(transfer_id=transfer_id)

    return render(request, 'app/view_transfer.html', {
        'transfer_items': transfer_items
    })


@login_required(login_url='/login/')
def transfer_receipt_pdf(request, transfer_id):
    itemtransfer = ItemTransfer.objects.filter(transfer_id=transfer_id)
    transfer = Transfer.objects.get(pk=transfer_id)
    receipt_no = str(transfer_id).zfill(6)
    user = request.user.username

    total = 0

    for item in itemtransfer:
        total = total + item.calculate_total

    context = {
        "items": itemtransfer,
        'transfer': transfer,
        "receipt_no": receipt_no,
        "user": user,
        "total": total
    }

    template = get_template('app/transfer_receipt.html')
    html = template.render(context)
    pdf = render_to_pdf('app/transfer_receipt.html', context)

    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "transfer_receipt_%s.pdf" % (receipt_no)
        content = "inline; filename='%s'" % (filename)
        download = request.GET.get("download")

        if download:
            content = "inline; filename='%s'" % (filename)

        response['Content-Disposition'] = content
        return response

    return HttpResponse("Not found")


@login_required(login_url='/login/')
def transfer_download_pdf(request, transfer_id):
    itemtransfer = ItemTransfer.objects.filter(transfer_id=transfer_id)
    transfer = Transfer.objects.get(pk=transfer_id)
    receipt_no = str(transfer_id).zfill(6)
    user = request.user.username

    total = 0

    for item in itemtransfer:
        total = total + item.calculate_total

    context = {
        "items": itemtransfer,
        'transfer': transfer,
        "receipt_no": receipt_no,
        "user": user,
        "total": total
    }

    template = get_template('app/transfer_receipt.html')
    html = template.render(context)
    pdf = render_to_pdf('app/transfer_receipt.html', context)

    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "transfer_receipt_%s.pdf" % (receipt_no)
        content = "attachment; filename='%s'" % (filename)
        download = request.GET.get("download")

        if download:
            content = "attachment; filename='%s'" % (filename)

        response['Content-Disposition'] = content
        return response

    return HttpResponse("Not found")


@login_required(login_url='/login/')
def arrival_list(request):
    arrivals = Arrival.objects.filter(is_active=True)

    return render(request, 'app/arrival.html', {
        'arrivals': arrivals
    })


@login_required(login_url='/login/')
@user_can_encode
def add_arrival(request):
    itemloc = ItemLocation.objects.all()
    arrivalForm = ArrivalForm(request.POST or None)
    formset = formset_factory(ItemArrivalForm, formset=ArrivalFormset)
    arrivalFormset = formset(request.POST or None)
    success = False

    if arrivalForm.is_valid() and arrivalFormset.is_valid():
        form1 = arrivalForm.save(commit=False)
        user = UserAccount.objects.get(username=request.user)
        arrival_location = Location.objects.get(name="WAREHOUSE")
        type = arrivalForm.cleaned_data['type']
        form1.location = arrival_location
        form1.user = user
        form1.save()
        arrival_id = form1

        for form in arrivalFormset:
            arrival = arrival_id

            item = form.cleaned_data['item']
            quantity = form.cleaned_data['quantity']
            i = ItemArrival.objects.create(item=item, arrival=arrival, quantity=quantity)

            for loc in itemloc:
                if loc.location == arrival_location and loc.item == item:
                    current_quantity = loc.current_stock
                    loc_quantity = current_quantity + quantity
                    loc.current_stock = loc_quantity
                    loc.save()
                    i.save()

            success = True

        return HttpResponseRedirect(reverse('receipt_arrival_pdf', args=(arrival.id,)))

    return render(request, 'app/add_arrival.html', {
        'formset': arrivalFormset,
        'ArrivalForm': arrivalForm,
        'success': success,
    })


@login_required(login_url='/login/')
def specific_arrivalItems(request, arrival_id):
    arrival_items = ItemArrival.objects.filter(arrival_id=arrival_id)

    return render(request, 'app/view_arrivals.html', {
        'arrival_items': arrival_items
    })


@login_required(login_url='/login/')
@user_can_view
def delete_arrival(request, arrival_id):
    arrival = Arrival.objects.get(pk=arrival_id)

    arrival.is_active = False
    arrival.save()

    return HttpResponseRedirect(reverse('arrivals'))


@login_required(login_url='/login/')
@user_can_encode
def update_arrival(request, arrival_id):
    loc = Location.objects.get(name="WAREHOUSE")
    itemloc = ItemLocation.objects.filter(location_id=loc.id)
    itemarrival = ItemArrival.objects.filter(arrival_id=arrival_id)
    arrival = Arrival.objects.get(id=arrival_id)
    ArrivalFormset = modelformset_factory(ItemArrival, form=ItemArrivalForm, fields=('item', 'quantity'), extra=0)
    data = request.POST or None
    formset = ArrivalFormset(data=data, queryset=itemarrival)
    details = {'type': arrival.type,
               'supplier': arrival.supplier,
               'receipt_no': arrival.receipt_no}

    arrivalForm = ArrivalForm(request.POST or None, initial=details)

    if request.method == "POST" and formset.is_valid() and arrivalForm.is_valid():
        form = arrivalForm.save(commit=False)
        supplier = arrivalForm.cleaned_data['supplier']
        type = arrivalForm.cleaned_data['type']
        receipt_no = arrivalForm.cleaned_data['receipt_no']

        arrival.supplier = supplier
        arrival.type = type
        arrival.receipt_no = receipt_no
        arrival.save()

        for form in formset:
            item = form.cleaned_data['item']
            quantity = form.cleaned_data['quantity']

            for i in itemloc:
                if i.item == item:
                    stock = i.current_stock
                    itemarr = itemarrival.get(item=item)
                    current_stock = stock - itemarr.quantity
                    i.current_stock = current_stock + quantity
                    i.save()

        formset.save()
        return HttpResponseRedirect(reverse('arrivals'))

    return render(request, 'app/update_arrival.html', {
        'formset': formset,
        'arrivalForm': arrivalForm
    })


@login_required(login_url='/login/')
def view_inventory(request):
    current_location = Location.objects.get(name="WAREHOUSE")
    location = ItemLocation.objects.filter(location_id=current_location.id)
    items = location.filter(is_active=True)
    search_date = datetime.now()
    success = False
    error = False
    arrival_items = []
    transfer_items = []

    if request.method == 'POST':
        try:
            input_date = request.POST.get('date', '')

            date,time = input_date.split('T')

            search_date = date + ' ' + time

            # list of arrival items before the search date
            arrival_items = ItemArrival.objects.extra(where = ["modified_date <= %s"], params = [search_date])


            
            # list of transferred items before the search date
            transfer_items = ItemTransfer.objects.extra(where = ["modified_date <= %s"], params = [search_date])
            
            # list of items in a location
            items = ItemLocation.objects.filter(location_id = current_location.id, is_active = True)


            for i in items:
                i.current_stock = 0
            

            # add all the arrival items before the search date
            for item in items:
                for arr_item in arrival_items:
                    if arr_item.item_id == item.item_id:
                        item.current_stock = item.current_stock + arr_item.quantity


            # subtract all the transfer items before the search date
            for item in items:
                for tran_item in transfer_items:
                    if tran_item.item_id == item.item_id:
                        item.current_stock = item.current_stock - tran_item.quantity


            success = True
        except Exception as e:
            error = True


    return render(request, 'app/view_inventory.html', {
        'items': items,
        'current_location': current_location, 
        'date' : search_date,
        'success': success,
        'error': error,
        'location': location

    })


@login_required(login_url='/login/')
def update_inventory(request, itemloc_id):
    item = ItemLocation.objects.get(pk=itemloc_id)
    loc_id = item.location_id

    if request.method == 'POST':
        item.current_stock = request.POST.get('update_item_currentstock', '')
        item.save()

        success = True
        return HttpResponseRedirect(reverse('view_inventory', args=(loc_id,)))

    return render(request, 'app/update_locitem.html', {
        'item': item,
    })


@login_required(login_url='/login/')
@user_can_view
def delete_inventory(request, itemloc_id):
    item = ItemLocation.objects.get(pk=itemloc_id)
    item.is_active = False
    item.save()

    loc_id = item.location.id

    return HttpResponseRedirect(reverse('view_inventory', args=(loc_id,)))


@login_required(login_url='/login/')
@user_can_encode
def create_transfer(request):
    items_list = Item.objects.all()
    itemloc = ItemLocation.objects.all()
    transferForm = TransferForm(request.POST or None)
    formset = formset_factory(ItemTransferForm, formset=ItemTransferFormset, extra=1)
    transferFormset = formset(request.POST or None)

    if transferForm.is_valid() and transferFormset.is_valid():
        p = transferForm.save(commit=False)
        source = Location.objects.get(name="WAREHOUSE")
        destination = transferForm.cleaned_data['To']
        user = UserAccount.objects.get(username=request.user)
        p.From = source
        p.user = user
        p.save()
        transfer_id = p

        for form in transferFormset:
            transfer = transfer_id
            item = form.cleaned_data['item']
            quantity = form.cleaned_data['quantity']
            i = ItemTransfer(item=item, quantity=quantity, transfer=transfer)

            for loc in itemloc:
                if loc.location == source and loc.item == item:
                    quantity_current = loc.current_stock

                    if quantity <= quantity_current:
                        decremented = quantity_current - quantity
                        loc.current_stock = decremented
                        loc.save()

                        for loct in itemloc:
                            if loct.location == destination and loct.item == item:
                                quantity_current = loct.current_stock
                                incremented = quantity_current + quantity
                                loct.current_stock = incremented
                                loct.save()
                                i.save()

        return HttpResponseRedirect(reverse('transfers'))

    return render(request, 'app/add_transfer.html', {
        'items_list': items_list,
        'TransferForm': transferForm,
        'formset': transferFormset,
        'itemloc': itemloc
    })


@login_required(login_url='/login/')
@user_can_encode
def processitems_list(request):
    items = Item.objects.filter(is_active=True)
    process_items = items.filter(type="Processed")

    return render(request, 'app/process_items.html', {
        'items': process_items
    })


@login_required(login_url='/login/')
@user_can_encode
def create_recipe(request, item_id):
    process_item = Item.objects.get(id=item_id)
    processitemForm = ProcessItemForm(request.POST or None)
    formset = formset_factory(IngredientsForm, formset=ProcessItemFormset)
    processitemFormset = formset(request.POST or None)

    if processitemForm.is_valid() and processitemFormset.is_valid():
        form1 = processitemForm.save(commit=False)
        form1.item = process_item
        form1.save()

        process_item.has_recipe = True
        process_item.save()

        process_id = form1
        for form in processitemFormset:
            processitem = process_id
            item = form.cleaned_data['item']
            i = Ingredients.objects.create(item=item, processed_item=processitem)
            i.save()

        return HttpResponseRedirect(reverse('process_items'))

    return render(request, 'app/add_process_item.html', {
        'formset': processitemFormset,
        'ProcessItemForm': processitemForm,
        'item': process_item
    })


@login_required(login_url='/login/')
@user_can_encode
def update_recipe(request, item_id):
    recipe = Recipe.objects.get(item_id=item_id)
    rec_id = recipe.id
    recipe_item = Ingredients.objects.filter(processed_item=rec_id)
    ProcessFormset = modelformset_factory(Ingredients,form=IngredientsForm, fields=('item',), extra=0)
    data = request.POST or None
    formset = ProcessFormset(data=data, queryset=recipe_item, prefix='process')

    processitemForm = ProcessItemForm(request.POST or None)
    formset1 = formset_factory(IngredientsForm, formset=ProcessItemFormset)
    processitemFormset = formset1(request.POST or None)

    if request.method == "POST" and formset.is_valid():
        formset.save()

        if processitemFormset.is_valid():
            for form in processitemFormset:
                item = form.cleaned_data['item']
                i = Ingredients.objects.create(item=item, processed_item_id=rec_id)
                i.save()

        return HttpResponseRedirect(reverse('process_items'))


    return render(request,'app/update_recipe.html', {
        'formset': formset,
        'formset1': processitemFormset
    })


def delete_ingredient(request, item_id):
    ingredient = Ingredients.objects.get(item_id=item_id)
    processed_id = ingredient.processed_item_id
    item = Recipe.objects.get(id=processed_id)
    recipe_id = item.item_id
    ingredient.delete()

    return HttpResponseRedirect(reverse('update_recipe', args=(recipe_id,)))


def process_item(request, item_id):
    ingredients = Ingredients.objects.filter(is_active=True)
    location = Location.objects.get(name="WAREHOUSE")
    itemloc = ItemLocation.objects.filter(is_active=True)
    recipe_item = Recipe.objects.get(item_id=item_id)
    items = ingredients.filter(processed_item=recipe_item.id)

    processForm = ProcessForm(request.POST or None)
    formset = formset_factory(ItemIngredientForm, formset=ProcessFormset, extra=0)
    processFormset = formset(request.POST or None, initial=[{'item': x.item_id} for x in items])

    if processForm.is_valid() and processFormset.is_valid():
        form1 = processForm.save(commit=False)
        form1.location = location
        item = Item.objects.get(id=item_id)
        form1.item = item
        quantity_yield = processForm.cleaned_data['quantity_yield']
        form1.save()

        for loc in itemloc:
            if loc.location == location and loc.item == item:
                loc_quantity = loc.current_stock
                loc_total = loc_quantity + quantity_yield
                loc.current_stock = loc_total
                loc.save()

        process_id = form1
        for form in processFormset:
            processitem = process_id
            item = form.cleaned_data['item']
            quantity = form.cleaned_data['quantity']
            i = ItemIngredient.objects.create(item=item, quantity=quantity, process=processitem)
            i.save()

            for loc in itemloc:
                if loc.location == location and loc.item == item:
                    loc_quantity = loc.current_stock
                    loc_total = loc_quantity - quantity
                    loc.current_stock = loc_total
                    loc.save()

        return HttpResponseRedirect(reverse('process_items'))

    return render(request, 'app/add_process.html', {
        'formset': processFormset,
        'recipe_item': recipe_item,
        'items': items,
        'ProcessForm': processForm
    })


@login_required(login_url='/login/')
@user_can_print
def receipt_arrival_pdf(request, arrival_id):
    arrival_items = ItemArrival.objects.filter(arrival_id=arrival_id)
    arrival = Arrival.objects.get(pk=arrival_id)
    receipt_no = str(arrival_id).zfill(6)
    total = 0

    for item in arrival_items:
        total = total + item.calculate_total

    context = {
        "items": arrival_items,
        "arrival": arrival,
        "total": total,
        "receipt_no": receipt_no
    }

    if arrival.type == "cash":
        template = get_template('app/cash_arrival_receipt.html')

        html = template.render(context)
        pdf = render_to_pdf('app/cash_arrival_receipt.html', context)

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "cash_arrival_receipt_%s.pdf" % (receipt_no)
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")

            if download:
                content = "inline; filename='%s'" % (filename)

            response['Content-Disposition'] = content
            return response

        return HttpResponse("Not found")

    else:
        template = get_template('app/payable_arrival_receipt.html')

        html = template.render(context)
        pdf = render_to_pdf('app/payable_arrival_receipt.html', context)

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "payable_arrival_receipt_%s.pdf" % (receipt_no)
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")

            if download:
                content = "inline; filename='%s'" % (filename)

            response['Content-Disposition'] = content
            return response

        return HttpResponse("Not found")


@login_required(login_url='/login/')
@user_can_print
def download_receipt_arrival(request, arrival_id):
    arrival_items = ItemArrival.objects.filter(arrival_id=arrival_id)
    arrival = Arrival.objects.get(pk=arrival_id)
    receipt_no = str(arrival_id).zfill(6)
    total = 0

    for item in arrival_items:
        total = total + item.calculate_total

    context = {
        "items": arrival_items,
        "arrival": arrival,
        "total": total,
        "receipt_no": receipt_no
    }

    if arrival.type == "cash":
        template = get_template('app/cash_arrival_receipt.html')

        html = template.render(context)
        pdf = render_to_pdf('app/cash_arrival_receipt.html', context)

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "cash_arrival_receipt_%s.pdf" % (receipt_no)
            content = "attachment; filename='%s'" % (filename)
            download = request.GET.get("download")

            if download:
                content = "attachment; filename='%s'" % (filename)

            response['Content-Disposition'] = content
            return response

        return HttpResponse("Not found")

    else:
        template = get_template('app/payable_arrival_receipt.html')

        html = template.render(context)
        pdf = render_to_pdf('app/payable_arrival_receipt.html', context)

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "payable_arrival_receipt_%s.pdf" % (receipt_no)
            content = "attachment; filename='%s'" % (filename)
            download = request.GET.get("download")

            if download:
                content = "attachment; filename='%s'" % (filename)

            response['Content-Disposition'] = content
            return response

        return HttpResponse("Not found")


@login_required(login_url='/login/')
@user_can_view
def employees_list(request):
    employees = Employee.objects.filter(is_active=True)
    success = False
    error = False

    if request.method == 'POST':
        try:
            employee_name = request.POST.get('employee_name', '')
            employee_position = request.POST.get('employee_position', '')
            employee_address = request.POST.get('employee_address', '')
            employee_number = request.POST.get('employee_number', '')

            i = Employee.objects.create(name=employee_name, position=employee_position, address=employee_address,
                                        phone=employee_number)
            i.save()

            success = True
        except:
            error = True

    return render(request, 'app/employees.html', {
        'employees': employees
    })


@login_required(login_url='/login/')
@user_can_view
def update_employee(request, employee_id):
    person = Employee.objects.get(pk=employee_id)

    if request.method == 'POST':
        person.name = request.POST.get('update_employee_name', '')
        person.position = request.POST.get('update_employee_position', '')
        person.address = request.POST.get('update_employee_address', '')
        person.phone = request.POST.get('update_employee_phone', '')
        person.save()

        return HttpResponseRedirect(reverse('employees_list'))

    return render(request, 'app/update_employee.html', {
        'person': person,
    })


@login_required(login_url='/login/')
@user_can_view
def delete_employee(request, employee_id):
    employee = Employee.objects.get(pk=employee_id)

    employee.is_active = False
    employee.save()

    return HttpResponseRedirect(reverse('employees_list'))


@login_required(login_url='/login/')
def reports(request):
    reports = Report.objects.filter(is_active=True)
    checklist = Checklist.objects.filter(is_active=True)

    user = request.user
    items = Item.objects.filter(is_active=True)
    today = datetime.now().date()

    if Report.objects.filter(created_date__contains=today):
        has_report=True
    else:
        has_report=False

    if request.method == 'POST':
        from_date = request.POST.get('from_date', '')
        to_date = request.POST.get('to_date', '')
        start_date = datetime.strptime(from_date, '%Y-%m-%d').date()
        end_day= datetime.strptime(to_date, '%Y-%m-%d').date() + timedelta(days=1)
        start= start_date.strftime('%Y-%m-%d')
        end= end_day.strftime('%Y-%m-%d')

        loc = Location.objects.filter(is_active=True)
        locations = loc.exclude(name="WAREHOUSE")
        suppliers = Supplier.objects.filter(is_active=True)
        cols = len(locations) + len(suppliers) + 3

        info = Item.objects.raw('''Select I.id, I.name, A.created_date , A.E_LIM, A.Bado, A.GAMA,T.MAIN, T.B1, T.B2, E.beg, E.end From app_item I
                                   join (Select S.created_date, item_id,
                                         SUM(CASE WHEN (Ar.supplier_id = '1') THEN quantity ELSE 0 END) AS E_LIM,
                                         SUM(CASE WHEN (Ar.supplier_id= '2') THEN quantity  ELSE 0 END) AS Bado,
                                         SUM(CASE WHEN (Ar.supplier_id='3') THEN quantity ELSE 0 END) AS GAMA
                                         from app_itemarrival S
                                         join (select supplier_id, id , created_date from app_arrival) 
                                         Ar on Ar.id = S.arrival_id
                                         where S.created_date >= %s and  S.created_date <=%s
                                         group by date(S.created_date), item_id)
								   A On I.id = A.item_id
								   join (Select * from app_itemexpense where created_date >= %s and  created_date <=%s  group by item_id, date(created_date))
								   E on I.id = E.item_id
								   left join(Select S.created_date, item_id,
                                        SUM(CASE WHEN (A.To_id = '1') THEN quantity ELSE 0 END) AS MAIN,
                                        SUM(CASE WHEN (A.To_id= '2') THEN quantity  ELSE 0 END) AS B1,
                                        SUM(CASE WHEN (A.to_id='3') THEN quantity ELSE 0 END) AS B2
                                        from app_itemtransfer S
                                        join (select To_id, id , created_date from app_transfer)
                                        A on A.id = S.transfer_id
                                        where S.created_date >= %s and  S.created_date <=%s
                                        group by date(S.created_date), item_id) T on T.item_id = I.id
                                  group by I.id, Date(A.created_date)''', [start, end, start, end, start, end])

        context = {'items': items,
                   'user': user,
                   'date': today,
                   'locations': locations,
                   'suppliers': suppliers,
                   'colslen': cols,
                   'to': end,
                   'info': info
                   }

        template = get_template('app/weekly_report.html')
        html = template.render(context)
        pdf = render_to_pdf('app/weekly_report.html', context)

        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "weekly_report_%s.pdf"
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")

        if download:
            content = "inline; filename='%s'" % (filename)
            response['Content-Disposition'] = content

        return response


    return render(request, 'app/sales_report.html', {
        'reports': reports,
        'checklist': checklist,
        'has_report': has_report
    })


@login_required(login_url='/login/')
@user_can_encode
@user_can_view
def all_expenses(request):
    expenses = Expenses.objects.filter(is_active=True)

    today = datetime.now().date()
    if request.method == 'POST':
        from_date = request.POST.get('from_date', '')
        to_date = request.POST.get('to_date', '')
        start_date = datetime.strptime(from_date, '%Y-%m-%d').date()
        end_day = datetime.strptime(to_date, '%Y-%m-%d').date() + timedelta(days=1)
        start = start_date.strftime('%Y-%m-%d')
        end = end_day.strftime('%Y-%m-%d')

        weekly_expense = Expenses.objects.raw('''Select id, sum(expenses) as expenses, sum(amount) as amount, date(created_date) as date_created from app_expenses where 
                                                date_created >=%s and date_created<= %s group by date_created''', [start, end])

        total_expense = 0
        total_amount = 0

        for i in weekly_expense:
            total_expense = total_expense + i.expenses
            total_amount = total_amount + i.amount

        return render(request, 'app/weekly_expenses.html', {
            'weekly_expense': weekly_expense,
            'total_expense': total_expense,
            'total_amount': total_amount
        })

    return render(request, 'app/expenses.html', {
        'all_expenses': expenses
    })


@login_required(login_url='/login/')
@user_can_encode
@user_can_view
def create_report(request, check_id):
    items = ChecklistField.objects.filter(checklist_id=check_id)
    itemloc = ItemLocation.objects.filter(is_active=True)

    reportForm = ReportForm(request.POST or None)
    formset = formset_factory(ItemExpenseForm, formset=ItemExpenseFormset, extra=0)
    formset1 = formset_factory(ExpenseForm, formset=ExpenseFormset)
    itemexpenseFormset = formset(request.POST or None, initial=[{'item': x.item_id} for x in items],
                                 prefix='itemexpense')
    expenseFormset = formset1(request.POST or None)
    error = False

    if reportForm.is_valid() and itemexpenseFormset.is_valid():
        form1 = reportForm.save(commit=False)
        location = reportForm.cleaned_data['location']
        user = UserAccount.objects.get(username=request.user)
        form1.user = user
        form1.save()
        report_id = form1

        today = datetime.now()
        for form in itemexpenseFormset:
            rep_id = report_id
            item = form.cleaned_data['item']
            end = form.cleaned_data['end']

            report_loc = Location.objects.get(name="WAREHOUSE")
            if location.id == report_loc.id:
                stock = 0
                item_arrival = ItemArrival.objects.filter(item_id=item)
                for x in item_arrival:
                    if x.created_date.date() == today.date():
                        stock = stock + x.quantity
                    else:
                        stock = 0

                for loc in itemloc:
                    if loc.location == location and loc.item == item:
                        total = loc.current_stock
                        if loc.current_stock >= end:
                            loc.current_stock = end
                            loc.save()
                        else:
                            rep = Report.objects.get(id=rep_id.id)
                            rep.delete()
                            messages.warning(request, 'Current stock shouldnt be less than END!')
                            return HttpResponseRedirect(reverse('create_report', args=(check_id)))

                if total >= stock:
                    beg = total - stock
                else:
                    beg = total

                i = ItemExpense.objects.create(item=item, new=stock, beg=beg, total=total, end=end, report=rep_id)
                i.save()

            else:
                stock = 0
                itemtransfers = ItemTransfer.objects.filter(item_id=item)
                for x in itemtransfers:
                    transfer = Transfer.objects.get(id=x.transfer_id)
                    if x.created_date.date() == today.date() and transfer.To_id == location.id:
                        stock = stock + x.quantity

                for loc in itemloc:
                    if loc.location == location and loc.item == item:
                        current_stock = loc.current_stock
                        beg = current_stock - stock
                        if loc.current_stock >= end:
                            loc.current_stock = end
                            loc.save()
                        else:
                            rep = Report.objects.get(id=rep_id.id)
                            rep.delete()
                            messages.warning(request, 'Current stock shouldnt be less than END!')
                            return HttpResponseRedirect(reverse('create_report', args=(check_id)))

                if total >= stock:
                    beg = total - stock
                else:
                    beg = total

                i = ItemExpense.objects.create(item=item, new=stock, beg=beg, end=end, report=rep_id)
                i.save()

            if expenseFormset.is_valid():
                for form in expenseFormset:
                    rep_id = report_id
                    amount = form.cleaned_data['amount']
                    expense = form.cleaned_data['expenses']
                    x = Expenses.objects.create(amount=amount, expenses=expense, report=rep_id)
                    x.save()

        return HttpResponseRedirect(reverse('daily_report', args=(report_id.id,)))

    return render(request, 'app/add_report.html', {
        'checklist': items,
        'check_id': check_id,
        'ReportForm': reportForm,
        'formset1': itemexpenseFormset,
        'formset2': expenseFormset,
        'error': error
    })


@login_required(login_url='/login/')
@user_can_update_report
def update_report(request, report_id):
    loc = Location.objects.get(name="WAREHOUSE")
    itemloc = ItemLocation.objects.filter(location_id=loc.id)
    itemexpense = ItemExpense.objects.filter(report_id=report_id)
    expense = Expenses.objects.filter(report_id=report_id)
    report = Report.objects.get(id=report_id)
    ItemExpenseFormset = modelformset_factory(ItemExpense, form=ItemExpenseForm, fields=('item', 'end'), extra=0)
    ExpenseFormset = modelformset_factory(Expenses, form=ExpenseForm, fields=('expenses', 'amount'), extra=0)
    data = request.POST or None
    formset = ItemExpenseFormset(data=data, queryset=itemexpense)
    formset1 = ExpenseFormset(data=data, queryset=expense)

    if request.method == "POST" and formset.is_valid() and formset1.is_valid():

        for form in formset:
            item = form.cleaned_data['item']
            end = form.cleaned_data['end']

            for i in itemloc:
                if i.item == item:
                    if i.current_stock > end:
                        i.current_stock = end
                        i.save()
                    else:
                        messages.warning(request, 'Current stock shouldnt be less than END!')
                        return HttpResponseRedirect(reverse('update_report', args=(report_id)))

        formset.save()
        formset1.save()
        return HttpResponseRedirect(reverse('reports'))

    return render(request, 'app/update_report.html', {
        'formset': formset,
        'report': report,
        'formset1': formset1
    })


@login_required(login_url='/login/')
@user_can_print
def daily_report_pdf(request, report_id):
    receipt_no = str(report_id).zfill(6)
    user = request.user.username
    report = Report.objects.get(id=report_id)
    itemexpense = ItemExpense.objects.filter(report_id=report_id)
    expense = Expenses.objects.filter(report_id=report_id)

    total_expense = 0
    total_amount = 0

    for i in expense:
        total_expense = total_expense + i.expenses
        total_amount = total_amount + i.amount

    context = {
        "items": itemexpense,
        "expense": expense,
        "report": report,
        "receipt_no": receipt_no,
        "user": user,
        "amount": total_amount,
        "total_expense": total_expense

    }

    template = get_template('app/daily_report.html')
    html = template.render(context)
    pdf = render_to_pdf('app/daily_report.html', context)

    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "daily_report_%s.pdf" % (receipt_no)
        content = "inline; filename='%s'" % (filename)
        download = request.GET.get("download")

        if download:
            content = "inline; filename='%s'" % (filename)

        response['Content-Disposition'] = content
        return response

    return HttpResponse("Not found")


@login_required(login_url='/login/')
@user_can_print
def daily_report_pdf_download(request, report_id):
    receipt_no = str(report_id).zfill(6)
    user = request.user.username
    report = Report.objects.get(id=report_id)
    itemexpense = ItemExpense.objects.filter(report_id=report_id)
    expense = Expenses.objects.filter(report_id=report_id)

    context = {
        "items": itemexpense,
        "expense": expense,
        "report": report,
        "receipt_no": receipt_no,
        "user": user
    }

    template = get_template('app/daily_report.html')
    html = template.render(context)
    pdf = render_to_pdf('app/daily_report.html', context)

    if pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        filename = "daily_report_%s.pdf" % (receipt_no)
        content = "attachment; filename='%s'" % (filename)
        download = request.GET.get("download")

        if download:
            content = "attachment; filename='%s'" % (filename)

        response['Content-Disposition'] = content
        return response

    return HttpResponse("Not found")



@login_required(login_url='/login/')
def branch_items(request):
    unwanted_instance = Location.objects.get(name="WAREHOUSE")
    branches = Location.objects.exclude(pk=unwanted_instance.id)
    locations = branches.filter(is_active=True)

    return render(request, "app/branch_items.html", {
        "locations": locations
    })


@login_required(login_url='/login/')
def branch_inventory(request, branch_id):
    location = Location.objects.get(pk=branch_id)
    all_items = Item.objects.filter(is_active=True)
    branch_items = ItemLocation.objects.filter(location_id=branch_id)
    items = all_items.filter(type="Processed")
    itemloc = ItemLocation.objects.filter(location_id=branch_id)
    success=False
    error=False
    search_date = datetime.now()


    if request.method == 'POST':
        item = request.POST.get('item', '')

        if item:
            current_stock = 0

            item_instance = Item.objects.get(id=item)
            if branch_items.filter(item_id=item_instance).exists():
                error=True
            else:
                i = ItemLocation.objects.create(item=item_instance, location=location, current_stock=current_stock)
                i.save()
                success=True


        # FOR CURRENT STOCK DATE FILTER
        input_date = request.POST.get('branch_search_date', '')

        if input_date=='':
            search_date = datetime.now()

        else:

            date,time = input_date.split('T')

            search_date = date + ' ' + time
        
        # list of transferred items before the search date
        transfer_items = ItemTransfer.objects.filter(transfer_id__To = branch_id, transfer_id__modified_date__lte = search_date)

        for i in itemloc:
            i.current_stock = 0

        # subtract all the transfer items before the search date
        for item in itemloc:
            for tran_item in transfer_items:
                if tran_item.item_id == item.item_id:
                    item.current_stock = item.current_stock + tran_item.quantity


    return render(request, "app/branch_inventory.html", {
        "current_location": location,
        "items": items,
        "itemloc": itemloc,
        "success": success,
        "error": error,
        "date": search_date,
    })


@login_required(login_url='/login/')
@user_can_update_arrival
def permission_update_arrival(request):
    user = request.user
    message = "Update Delivery"
    i = Permission.objects.create(user=user, message=message, requested_report=False, requested_arrival=True)
    i.save()

    return HttpResponseRedirect(reverse('arrivals'))


@login_required(login_url='/login/')
@user_can_encode
def permission_update_report(request):
    user = request.user
    message = "Update Sales Report"
    i = Permission.objects.create(user=user, message=message, requested_report=True, requested_arrival=False)
    i.save()

    return HttpResponseRedirect(reverse('reports'))


@login_required(login_url='/login/')
@user_can_encode
def permissions(request):
    requests = Permission.objects.filter(is_active=True)

    return render(request, 'app/permissions.html', {
        'permissions': requests
    })


@login_required(login_url='/login/')
@user_can_view
def approve_permission(request, user_id, request_id):
    user = UserAccount.objects.get(id=user_id)
    request = Permission.objects.get(id=request_id)

    if request.requested_report:
        user.can_update_report = True
        user.save()
    else:
        user.can_update_arrival = True
        user.save()

    request.delete()

    return HttpResponseRedirect(reverse('permissions'))
