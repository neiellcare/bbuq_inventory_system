from django.core.exceptions import PermissionDenied

def user_can_view(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_active and request.user.is_superuser:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def user_can_print(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_active and request.user.can_print:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def user_can_encode(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_active and request.user.can_encode:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def user_can_update_arrival(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_active and request.user.can_update_arrival:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def user_can_update_report(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_active and request.user.can_update_report:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


