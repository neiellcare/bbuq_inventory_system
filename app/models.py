# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import connection, models


class MyManager(models.Manager):
    def raw_as_qs(self, raw_query, params=()):
        """Execute a raw query and return a QuerySet.  The first column in the
        result set must be the id field for the model.
        :type raw_query: str | unicode
        :type params: tuple[T] | dict[str | unicode, T]
        :rtype: django.db.models.query.QuerySet
        """
        cursor = connection.cursor()
        try:
            cursor.execute(raw_query, params)
            return self.filter(id__in=(x[0] for x in cursor))
        finally:
            cursor.close()


class BasicModels(models.Model):
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class AccountManager(BaseUserManager):
    def create_user(self, username, password=None, **kwargs):
        if not username:
            raise ValueError("Users must have a valid username!")

        account = self.model(
            username=username,
            firstname=kwargs.get('firstname', None),
            middle_initial=kwargs.get('middle_initial', None),
            lastname=kwargs.get('lastname', None),
            gender=kwargs.get('gender', None),
            position=kwargs.get('position', None),
            is_staff=kwargs.get('is_staff', True),
        )
        account.set_password(password)
        account.save()

        return account

    def create_superuser(self, username, password=None, **kwargs):
        account = self.create_user(username, password, **kwargs)
        account.is_admin= True
        account.can_encode=True
        account.can_print=True
        account.can_update_arrival=True
        account.can_update_report=True
        account.save()
        return account


class UserAccount(AbstractBaseUser):
    username = models.CharField(unique=True, max_length=50)

    is_staff = models.BooleanField(default=False)
    can_print = models.BooleanField(default=False)
    can_encode = models.BooleanField(default=False)
    can_update_arrival = models.BooleanField(default=False)
    can_update_report = models.BooleanField(default=False)

    firstname = models.CharField(max_length=200, blank=True, null=True)
    middle_initial = models.CharField(max_length=2, blank=True, null=True)
    lastname = models.CharField(max_length=200, blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True)
    position = models.CharField(max_length=200, blank=True, null=True)

    is_admin = models.BooleanField(default=False)

    objects = AccountManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['firstname', 'lastname']

    def get_full_name(self):
        return (self.firstname + " " + self.lastname)

    def get_short_name(self):
        return self.username

    @property
    def is_superuser(self):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    def __unicode__(self):
        return self.username


class Employee(BasicModels):
    name = models.CharField(max_length=100, null=True, blank=True, unique=True)
    position = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)
    has_account = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Categories(BasicModels):
    name = models.CharField(max_length=100, null=True, blank=True, unique=True)

    def __str__(self):
        return self.name

class Item(BasicModels):
    objects = MyManager()
    types = (
        ('raw', 'raw'),
        ('processed', 'processed'),
        ('process&raw', 'process&raw'),
    )
    name = models.CharField(max_length=100, null=True, blank=True, unique=True)
    price = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15)
    type = models.CharField(max_length=10, choices=types, default='raw')
    category = models.ForeignKey(Categories)
    has_recipe = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering=['name']


class Location(BasicModels):
    name = models.CharField(max_length=100, null=True, blank=True, unique=True)
    address = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name


class ItemLocation(models.Model):
    item = models.ForeignKey(Item)
    location = models.ForeignKey(Location)
    current_stock = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.item.name


class Supplier(BasicModels):
    name = models.CharField(max_length=100, null=True, blank=True, unique=True)
    address = models.CharField(max_length=200, null=True)
    phone = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name


class Transfer(BasicModels):
    From = models.ForeignKey(Location, related_name="from+")
    To = models.ForeignKey(Location, related_name="to+")
    items = models.ManyToManyField(Item, through="ItemTransfer")
    user = models.ForeignKey(UserAccount)


class ItemTransfer(BasicModels):
    quantity = models.IntegerField(default=0)
    item = models.ForeignKey(Item)
    transfer = models.ForeignKey(Transfer, null=True, blank=True)

    @property
    def calculate_total(self):
        return self.item.price * self.quantity


class Arrival(BasicModels):
    types = (
        ('cash', 'cash'),
        ('payable', 'payable'),
    )
    type = models.CharField(max_length=10, choices=types, default='cash')
    items = models.ManyToManyField(Item, through='ItemArrival')
    supplier = models.ForeignKey(Supplier)
    location = models.ForeignKey(Location)
    receipt_no = models.IntegerField()
    user = models.ForeignKey(UserAccount)

    class Meta:
        ordering = ['-created_date']

    def __unicode__(self):
        return self.id

    def items_list(self):
        return [a.item for a in self.items.all()]


    @staticmethod
    def apply_filter(start, end, supplier):
        items = Arrival.objects.filter(supplier=supplier).filter(created_date__gt=start, created_date__lt=end)
        return items

    @property
    def get_grand_total(self):
        grand_total = 0
        items_set = self.items.all()
        for item in items_set:
            grand_total = grand_total + item.calculate_total
        return grand_total



class ItemArrival(BasicModels):
    item = models.ForeignKey(Item)
    arrival = models.ForeignKey(Arrival)
    quantity = models.IntegerField()

    def __unicode__(self):
        return " ".join((unicode(self.item.name), unicode(self.quantity)))

    @property
    def calculate_total(self):
        return self.item.price * self.quantity


class Checklist(BasicModels):
    name = models.CharField(max_length=255)
    items = models.ManyToManyField(Item, through='ChecklistField')


class ChecklistField(BasicModels):
    item = models.ForeignKey(Item, null=True, blank=True)
    display_order = models.IntegerField()
    checklist = models.ForeignKey(Checklist, null=True, blank=True)

    class Meta:
        ordering=['display_order']


class Recipe(BasicModels):
    item = models.ForeignKey(Item, related_name='item+')
    ingredient = models.ManyToManyField(Item, through='Ingredients')


class Ingredients(BasicModels):
    item = models.ForeignKey(Item)
    processed_item = models.ForeignKey(Recipe, null=True, blank=True)

class Process(BasicModels):
    location = models.ForeignKey(Location)
    item = models.ForeignKey(Item, related_name='item+')
    quantity_yield = models.IntegerField()
    ingredient = models.ManyToManyField(Item, through='ItemIngredient')

class ItemIngredient(BasicModels):
    item = models.ForeignKey(Item)
    quantity = models.IntegerField()
    process = models.ForeignKey(Process, null=True, blank=True)


class Report(BasicModels):
    name = models.CharField(max_length=100, null=True, blank=True, unique=True)
    location = models.ForeignKey(Location)
    user = models.ForeignKey(UserAccount, null=True, blank=True)
    items = models.ManyToManyField(Item, through='ItemExpense')

class Expenses(BasicModels):
    expenses = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15, default=0)
    amount=models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=15, default=0)
    report = models.ForeignKey(Report, null=True, blank=True)

class ItemExpense(BasicModels):
    item= models.ForeignKey(Item)
    new = models.IntegerField()
    beg = models.IntegerField()
    total= models.IntegerField()
    end= models.IntegerField()
    report = models.ForeignKey(Report, null=True, blank=True)

class Permission(BasicModels):
    user= models.ForeignKey(UserAccount)
    message=models.TextField()
    requested_report = models.BooleanField(default=False)
    requested_arrival = models.BooleanField(default=False)

