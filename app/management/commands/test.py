from django.core.management.base import BaseCommand, CommandError
from app.models import *


class Command(BaseCommand):
    args = 'Populate Data'
    help = 'Pre-populate Data'

    def _create_data(self):
        loc = Location.objects.get(name="WAREHOUSE")
        items = Item.objects.filter(is_active=True)


        arrival =[
            Arrival(type='cash', supplier_id=2, location_id=1, receipt_no=3242, user_id=1),
        ]

        for i in arrival:
            arr = i.save()
            for x in items:
                y = ItemArrival.objects.create(item=x, arrival=i, quantity=10)
                y.save()

        report = [
            Report(name='Rep4', location_id=1, user_id=1),
        ]

        for i in report:
            rep = i.save()
            for x in items:
                y = ItemExpense.objects.create(item=x, new=10, beg=0, total=10, end=5, report=i)
                y.save()

        transfer =[
            Transfer(From_id=loc.id, To_id=1, user_id=1),
            Transfer(From_id=loc.id, To_id=2, user_id=1),
            Transfer(From_id=loc.id, To_id=3, user_id=1),
        ]

        for i in transfer:
            i.save()
            for x in items:
                y= ItemTransfer(item=x, quantity=10, transfer=i)
                y.save()


    def handle(self, *args, **options):
        self._create_data()