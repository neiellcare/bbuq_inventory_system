from django.core.management.base import BaseCommand, CommandError
from app.models import *


class Command(BaseCommand):
    args = 'Populate Data'
    help = 'Pre-populate Data'

    def _create_data(self):
        categories = [
            Categories(name='foods'),
            Categories(name='drinks'),
            Categories(name='others')]

        for cat in categories:
            cat.save()

        location = [
            Location( name='MAIN', address='Quezon Avenue Extension, Pala-o, Iligan City'),
            Location( name='B1', address='Villania Property, Roxas Avenue, Poblacion, Iligan City'),
            Location( name='B2', address='A. Monsubre Bldg., Gagara St. San Miguel, Iligan City'),
            Location( name='WAREHOUSE', address='Iligan City')]

        for loc in location:
          loc.save()
          
        item = [
            Item(name = 'PORK BBQ',price = 10.75,type = 'Raw', category_id = 1),
            Item(name = 'CHICKEN LEG',price = 46.25,type = 'Raw', category_id = 1),
            Item(name = 'CHICKEN BREAST',price = 51.00,type = 'Raw', category_id = 1),
            Item(name = 'CHICKEN LIVER',price = 14.00,type = 'Raw', category_id = 1),
            Item(name = 'CHICKEN GIZZARD',price = 14.00,type = 'Raw', category_id = 1),
            Item(name = 'RICE',price = 190.00,type = 'Raw', category_id = 1),
            Item(name = 'BANGUS',price = 100.00,type = 'Raw', category_id = 1),
            Item(name = 'BULALO',price = 52.00,type = 'Raw', category_id = 1),
            Item(name = 'CHORIZO',price = 25.00,type = 'Processed', category_id = 1),
            Item(name = 'PACK CHORIZO',price = 100.00,type = 'Processed', category_id = 1),
            Item(name = 'CHICHARON',price = 45.00,type = 'Processed', category_id = 1),
            Item(name = 'SPICY CHICKEN',price = 55.00,type = 'Processed', category_id = 1),
            Item(name = 'BLUE MARLIN',price = 65.00,type = 'Raw', category_id = 1),
            Item(name = 'PEPSI/7UP 8 OZ',price = 10.00,type = 'Processed', category_id = 2),
            Item(name = 'MOUNTAIN DEW',price = 12.00,type = 'Processed', category_id = 2),
            Item(name = 'PEPSI/7UP 1.5 L',price = 65.00,type = 'Processed', category_id = 2),
            Item(name = 'MINERAL WATER SMALL',price = 11.00,type = 'Processed', category_id = 2),
            Item(name = 'MINERAL WATER BIG',price = 17.00,type = 'Processed', category_id = 2),
            Item(name = 'SAN MIG LIGHT',price = 30.00,type = 'Processed', category_id = 2),
            Item(name = 'SAN MIG PILSEN',price = 26.25,type = 'Processed', category_id = 2),
            Item(name = 'LEMON',price = 45.00,type = 'Raw', category_id = 1),
            Item(name = 'SILI',price = 350.00,type = 'Raw', category_id = 1),
            Item(name = 'TOYO',price = 495.00,type = 'Processed', category_id = 1),
            Item(name = 'SUKA',price = 400.00,type = 'Processed', category_id = 1),
            Item(name = 'OIL',price = 70.00,type = 'Processed', category_id = 1),
            Item(name = 'PORK SAUCE',price = 102.00,type = 'Processed', category_id = 1),
            Item(name = 'CHICKEN SAUCE',price = 162.00,type = 'Processed', category_id = 1),
            Item(name = 'BBUQ EXPRESS',price = 20.00,type = 'Processed', category_id = 1),
            Item(name = 'DAHON(PLAIN)',price = 7.00,type = 'Processed', category_id = 3),
            Item(name = 'BOLCITA',price = 0.40,type = 'Processed', category_id = 3),
            Item(name = 'CB#5',price = 190.00,type = 'Processed', category_id = 3),
            Item(name = 'TINY',price = 190.00,type = 'Processed', category_id = 3),
            Item(name = 'TOYO WRAPPER',price = 35.00,type = 'Processed', category_id = 3),
            Item(name = 'TAPE',price = 25.00,type = 'Processed', category_id = 3),
            Item(name = 'ICE WRAPPER',price = 20.00,type = 'Processed', category_id = 3),
            Item(name = 'SANITIZER',price = 1100.00,type = 'Processed', category_id = 3),
            Item(name = 'POWDER',price = 15.00,type = 'Processed', category_id = 3),
            Item(name = 'O.S TAKE OUT',price = 380.00,type = 'Processed', category_id = 3),
            Item(name = 'JUMBO ROLL',price = 157.50,type = 'Processed', category_id = 3),
            Item(name = 'FORM',price = 25.00,type = 'Processed', category_id = 3),
            Item(name = 'KITCHEN THERMOMETER',price = 615.00,type = 'Processed', category_id = 3),
            Item(name = 'SOLANE',price = 850.00,type = 'Processed', category_id = 3),
            Item(name = 'LECHE PLAN',price = 31.00,type = 'Processed', category_id = 1),
            Item(name = 'PULLNAPS',price = 1300.00,type = 'Processed', category_id = 3),
            Item(name = 'STRAW. CHEESE CAKE',price = 67.00,type = 'Processed', category_id = 1),
            Item(name = 'OLING',price = 250.00,type = 'Processed', category_id = 3),
            Item(name = 'O.S D-IN',price = 400.00,type = 'Processed', category_id = 3),
            Item(name = 'B.BERRY CHEESE CAKE',price = 67.00,type = 'Processed', category_id = 1),
            Item(name = 'MINERAL 4 CREW',price = 35.00,type = 'Processed', category_id = 2)]

        for i in item:
            i.save()

        loc = Location.objects.get(name="WAREHOUSE")
        items = Item.objects.filter(is_active=True)

        for x in items:
            i = ItemLocation.objects.create(item=x,location=loc,current_stock=0,is_active=True)
            i.save()

        #for testing purposes only

        supplier = [
          Supplier(name = 'Estrella', address = 'Iligan City', phone = '')]

        for sup in supplier:
          sup.save()

        arrival =[
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=3242, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=456547, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=54623, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=7643, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=400284, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=549942, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=49294, user_id=1),
            Arrival(type='cash', supplier_id=1, location_id=1, receipt_no=324242, user_id=1),
        ]

        for i in arrival:
            arr = i.save()
            for x in item:
                y = ItemArrival.objects.create(item=x, arrival=i, quantity=10)
                y.save()

        report = [
            Report(name='Rep1', location_id=1, user_id=1),
            ]

        for i in report:
            rep = i.save()
            for x in item:
                y = ItemExpense.objects.create(item=x, new =10, beg=0, total=10, end=5, report=i)
                y.save()

        # employee = [
        #   Employee(name='',position='',address='',phone='')]

        # for emp in employee:
           #  emp.save()


    def handle(self, *args, **options):
        self._create_data()            