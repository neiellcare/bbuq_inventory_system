from django.conf.urls import url
from . import views

urlpatterns = [
    # Items
    url(r'^items/$', views.item_page, name='list_items'),
    url(r'^items/delete/(?P<item_id>[0-9]+)/$', views.delete_item, name='delete_item'),
    url(r'^items/update/(?P<item_id>[0-9]+)/$', views.update_item, name='update_item'),

    # Categories
    url(r'^categories/$', views.categories, name='categories'),
    url(r'^categories/update/(?P<category_id>[0-9]+)/$', views.update_category, name='update_category'),
    url(r'^categories/delete/(?P<category_id>[0-9]+)/$', views.delete_category, name='delete_category'),

    # Inventory
    url(r'^inventory/process_item/(?P<item_id>[0-9]+)/$', views.process_item, name='process_item'),
    url(r'^inventory/view/$', views.view_inventory, name='view_inventory'),
    url(r'^inventory/update/(?P<itemloc_id>[0-9]+)/$', views.update_inventory, name='update_inventory'),
    url(r'^inventory/delete/(?P<itemloc_id>[0-9]+)/$', views.delete_inventory, name='delete_inventory'),

    # Locations
    url(r'^locations/$', views.location_page, name='location_list'),
    url(r'^locations/delete/(?P<loc_id>[0-9]+)/$', views.delete_location, name='delete_location'),
    url(r'^locations/update/(?P<loc_id>[0-9]+)/$', views.update_location, name='update_location'),


    url(r'^process_items/$', views.processitems_list, name='process_items'),
    url(r'^process_items/create_recipe/(?P<item_id>[0-9]+)/$', views.create_recipe, name='create_recipe'),
    url(r'^process_items/update_recipe/(?P<item_id>[0-9]+)/$', views.update_recipe, name='update_recipe'),
    url(r'^process_items/process/(?P<item_id>[0-9]+)/$', views.process_item, name='process'),
    url(r'^process_items/delete_ingredient/(?P<item_id>[0-9]+)/$', views.delete_ingredient, name='delete_ingredient'),

    # Suppliers
    url(r'^suppliers/$', views.suppliers, name='suppliers'),
    url(r'^suppliers/update/(?P<supplier_id>[0-9]+)/$', views.update_supplier, name='update_supplier'),
    url(r'^suppliers/delete/(?P<supplier_id>[0-9]+)/$', views.delete_supplier, name='delete_supplier'),

    # Checklists
    url(r'^checklist/$', views.checklist_page, name='checklist'),
    url(r'^checklist/add$', views.add_checklist, name='add_checklist'),
    url(r'^checklist/(?P<check_id>[0-9]+)/$', views.specific_checklist, name='specific_checklist'),
    url(r'^checklist/delete/(?P<check_id>[0-9]+)/$$', views.delete_checklist, name='delete_checklist'),
    url(r'^checklist/update/(?P<check_id>[0-9]+)/$$', views.update_checklist, name='update_checklist'),
    url(r'^checklist/item/delete/(?P<item_id>[0-9]+)/$$', views.delete_check, name='delete_check'),

    url(r'^arrival/$', views.arrival_list, name='arrivals'),
    url(r'^arrival/add$', views.add_arrival, name='add_arrival'),
    url(r'^arrival/(?P<arrival_id>[0-9]+)/$', views.specific_arrivalItems, name='arrivalItems'),
    url(r'^arrival/delete/(?P<arrival_id>[0-9]+)/$', views.delete_arrival, name='delete_arrival'),
    url(r'^arrival/update/(?P<arrival_id>[0-9]+)/$', views.update_arrival, name='update_arrival'),

    url(r'^transfer/$', views.create_transfer, name='create_transfer'),
    # url(r'^arrival/payable/(?P<arrival_id>[0-9]+)/$', views.payable_arrival_pdf, name='payable_arrival_pdf'),
    url(r'^arrival/receipt/(?P<arrival_id>[0-9]+)/$', views.receipt_arrival_pdf, name='receipt_arrival_pdf'),
    url(r'^arrival/download/(?P<arrival_id>[0-9]+)/$', views.download_receipt_arrival, name='download_receipt_arrival'),

    # Employees
    url(r'^employees/$', views.employees_list, name='employees_list'),
    url(r'^employees/delete/(?P<employee_id>[0-9]+)/$$', views.delete_employee, name='delete_employee'),
    url(r'^employees/update/(?P<employee_id>[0-9]+)/$$', views.update_employee, name='update_employee'),
    url(r'^employees/create_account/(?P<employee_id>[0-9]+)/$$', views.create_account, name='create_account'),

    #Reports
    url(r'^reports/$', views.reports, name='reports'),
    url(r'^reports/create/(?P<check_id>[0-9]+)/$', views.create_report, name='create_report'),
    url(r'^reports/update/(?P<report_id>[0-9]+)/$', views.update_report, name='update_report'),
    url(r'^reports/create_report/(?P<report_id>[0-9]+)/$', views.daily_report_pdf, name='daily_report'),
    url(r'^reports/create_report_download/(?P<report_id>[0-9]+)/$', views.daily_report_pdf_download, name='daily_report_download'),

    url(r'^expenses/$', views.all_expenses, name='expenses'),

    #Transfer
    url(r'^transfers/$', views.transfer_list, name='transfers'),
    url(r'^transfers/(?P<transfer_id>[0-9]+)$', views.specific_transferItems, name='specific_transfer'),
    url(r'^transfers/receipt/(?P<transfer_id>[0-9]+)$', views.transfer_receipt_pdf, name='transfer_receipt'),
    url(r'^transfers/download/(?P<transfer_id>[0-9]+)$', views.transfer_download_pdf, name='transfer_download'),

    #Branch Items
    url(r'^branch_items/$', views.branch_items, name='branch_items'),
    url(r'^branch_items/inventory/(?P<branch_id>[0-9]+)$$', views.branch_inventory, name='branch_inventory'),

    #Permission
    url(r'^permission/send_arrival/$', views.permission_update_arrival, name='permission_arrival'),
    url(r'^permission/send_report/$', views.permission_update_report, name='permission_report'),
    url(r'^permission/approve/(?P<user_id>[0-9]+)/(?P<request_id>[0-9]+)$$', views.approve_permission, name='approve'),

    url(r'^permissions/$', views.permissions, name='permissions'),

    # Users
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),

    url(r'', views.index, name='index'),
]
