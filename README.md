# B-Bu-Q Inventory System #
### Requirements ###

* Django
* Django-Mathfilters
* xhtml2pdf

### Installation ###

Start with `git clone https://gitlab.com/neiellcare/bbuq_inventory_system.git`

```
    $ pip install -r requirements.txt
    $ python manage.py migrate
    $ python manage.py createsuperuser
    $ python manage.py runserver
```

### Development workflow ###

We are using the Git Flow
https://guides.github.com/introduction/flow/

The requested workflow would be:
When you start to work on a new ticket (assuming the previous one been finished and pushed):
```
git checkout master
git pull
git checkout -b <newbranchname>
```
'newbranchname' should be the Asana task name. For example:  git checkout -b task_feature
The recommendation is to do the git add and git commit -m "<what step has been done>" frequently.
If the task takes several days the recommended to do git push on every other day.
When the job declared on ticket is finished then do a git push
Then log in to gitlab.com and request a merge from the branch and assign it to Neiell
