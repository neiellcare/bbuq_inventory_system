function confirmItemDelete(item_id) {
    console.log(item_id);
    var r = confirm('Are you sure?');
    if (r == true)
        window.location = '/items/delete/' + item_id + '/';
    else
        window.location = '/items/';
}

function confirmArrivalDelete(arrival_id) {
    console.log(arrival_id);
    var r = confirm('Are you sure?');
    if (r == true)
        window.location = '/arrival/delete/' + arrival_id + '/';
    else
        window.location = '/arrival/';
}

function confirmCategoryDelete(category_id) {
    console.log(category_id);
    var r = confirm('Are you sure?');
    if (r == true)
        window.location = '/categories/delete/' + category_id + '/';
    else
        window.location = '/categories/';
}

function confirmSupplierDelete(supplier_id) {
    console.log(supplier_id);
    var r = confirm('Are you sure?');
    if (r == true)
        window.location = '/suppliers/delete/' + supplier_id + '/';
    else
        window.location = '/suppliers/';
}

function confirmLocationDelete(location_id) {
    console.log(location_id);
    var r = confirm('Are you sure?');
    if (r == true)
        window.location = '/locations/delete/' + location_id + '/';
    else
        window.location = '/locations/';
}

function confirmEmployeeDelete(employee_id) {
    var r = confirm('Are you sure?');
    if (r == true)
        window.location = '/employees/delete/' + employee_id + '/';
    else
        window.location = '/employees/';
}

function confirmInventoryDelete(itemloc_id) {
    var r = confirm('Are you sure?');
    if (r == true) {
        window.location = '/inventory/delete/' + itemloc_id + '/';
    }
}

function confirmChecklistDelete(check_id) {
    var r = confirm('Are you sure?');
    if (r == true) {
        window.location = '/checklist/delete/' + check_id + '/';
    }
}

function update_arrival(){
    var r = confirm('Request Permission?');
    if (r == true) {
        window.location = '/permission/send_arrival/';
    }
}

function update_report(){
    var r = confirm('Request Permission?');
    if (r == true) {
        window.location = '/permission/send_report/';
    }
}


$(function() {
  var oTable = $('#datatable').DataTable({
    "oLanguage": {
      "sSearch": "Filter Data"
    },
    "iDisplayLength": -1,
    "sPaginationType": "full_numbers",

  });




  $("#datepicker_from").datepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    

    return true;
  }
);